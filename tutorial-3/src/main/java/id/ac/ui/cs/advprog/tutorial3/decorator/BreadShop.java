package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.CrustySandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.NoCrustSandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThickBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThinBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.BeefMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cheese;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChickenMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChiliSauce;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cucumber;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Lettuce;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Tomato;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.TomatoSauce;


/**
 * Created by farah on 3/2/2018.
 */
public class BreadShop {
    public static void main(String[] args) {
        Food food1 = new CrustySandwich();
        System.out.println(food1.getDescription()
                + " $" + food1.cost());

        Food food2 = new NoCrustSandwich();
        food2 = new BeefMeat(food2);
        food2 = new Cheese(food2);
        food2 = new ChiliSauce(food2);
        food2 = new Tomato(food2);
        System.out.println(food2.getDescription()
                + " $" + food2.cost());

        Food food3 = new ThickBunBurger();
        food3 = new ChickenMeat(food3);
        food3 = new Cucumber(food3);
        food3 = new Lettuce(food3);
        food3 = new TomatoSauce(food3);
        System.out.println(food3.getDescription()
                + " $" + food3.cost());

        Food food4 = new ThinBunBurger();
        food4 = new BeefMeat(food4);
        System.out.println(food4.getDescription()
                + " $" + food4.cost());
    }
}
