package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

/**
 * Created by farah on 3/1/2018.
 */
public class UiUxDesigner extends Employees {
    public UiUxDesigner(String name, double salary) {
        this.name = name;
        if (salary < 90000.00) {
            throw new IllegalArgumentException();
        } else {
            this.salary = salary;
        }
        this.role = "UI/UX Designer";
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
