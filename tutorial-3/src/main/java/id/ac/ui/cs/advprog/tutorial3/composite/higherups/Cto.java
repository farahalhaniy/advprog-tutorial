package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

/**
 * Created by farah on 3/1/2018.
 */
public class Cto extends Employees {
    public Cto(String name, double salary) {
        this.name = name;
        if (salary < 100000.00) {
            throw new IllegalArgumentException();
        } else {
            this.salary = salary;
        }
        this.role = "CTO";
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
