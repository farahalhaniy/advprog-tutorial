package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

/**
 * Created by farah on 3/1/2018.
 */
public class FrontendProgrammer extends Employees {
    public FrontendProgrammer(String name, double salary) {
        this.name = name;
        if (salary < 30000.00) {
            throw new IllegalArgumentException();
        } else {
            this.salary = salary;
        }
        this.role = "Front End Programmer";
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
