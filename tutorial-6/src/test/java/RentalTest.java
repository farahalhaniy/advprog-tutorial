import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class RentalTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable
    private Movie movie;
    private Movie movie2;
    private Movie movie3;
    private Rental rent;
    private Rental newRent;
    private Rental childrenRent;

    @Before
    public void setUp() throws Exception {
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        movie2 = new Movie("Coco", Movie.NEW_RELEASE);
        movie3 = new Movie("Moana", Movie.CHILDREN);
        rent = new Rental(movie, 3);
        newRent = new Rental(movie2, 3);
        childrenRent = new Rental(movie3, 4);
    }

    @Test
    public void getMovie() {
        assertEquals(movie, rent.getMovie());
    }

    @Test
    public void getDaysRented() {
        assertEquals(3, rent.getDaysRented());
    }

    @Test
    public void charge() {
        assertEquals("3.5", "" + rent.charge());
        assertEquals("9.0", "" + newRent.charge());
        assertEquals("3.0", "" + childrenRent.charge());
    }

    @Test
    public void frequentRenterPoints() {
        assertEquals(1, rent.frequentRenterPoints());
        assertEquals(2, newRent.frequentRenterPoints());
    }
}