import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class MovieTest {

    // TODO: Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable
    private Movie movie;
    private Movie testSameMovie;
    private Movie testDifferentMovie;

    @Before
    public void setUp() throws Exception {
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        testSameMovie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        testDifferentMovie = new Movie("Coco", Movie.NEW_RELEASE);
    }

    @Test
    public void getTitle() {
        assertEquals("Who Killed Captain Alex?", movie.getTitle());
    }

    @Test
    public void setTitle() {
        movie.setTitle("Bad Black");

        assertEquals("Bad Black", movie.getTitle());
    }

    @Test
    public void getPriceCode() {
        assertEquals(Movie.REGULAR, movie.getPriceCode());
    }

    @Test
    public void setPriceCode() {
        movie.setPriceCode(Movie.CHILDREN);
        assertEquals(Movie.CHILDREN, movie.getPriceCode());
    }

    @Test
    public void equals() {
        assertTrue(movie.equals(testSameMovie));
        assertFalse(movie.equals(testDifferentMovie));
        assertFalse(movie.equals(null));
    }

    @Test
    public void hashCodeTest() {
        assertEquals(movie.hashCode(), testSameMovie.hashCode());
        assertNotEquals(movie.hashCode(), testDifferentMovie.hashCode());
    }
}