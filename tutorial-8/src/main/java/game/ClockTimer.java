package game;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by farah on 4/13/2018.
 */
public class ClockTimer implements Runnable {
    private AtomicInteger countForScore;
    private AtomicInteger countForTime;

    public ClockTimer() {
        this.countForScore = new AtomicInteger(0);
        this.countForTime = new AtomicInteger(0);
    }

    public synchronized void run() {
        try {
            Thread.sleep(1000);
            this.countForScore.getAndIncrement();
            this.countForTime.getAndIncrement();
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public synchronized AtomicInteger getCountForScore() {
        return countForScore;
    }

    public synchronized AtomicInteger getCountForTime() {
        return countForTime;
    }

    public synchronized int calculateAnsInsideThresHold(int score) {
        return (int) (score * 0.1);
    }

    public synchronized int calculateAnsOutsideThresHold(int score) {
        return (int) (score * 0.05);
    }
}
