package game;

import java.util.Scanner;

/**
 * Created by farah on 4/13/2018.
 */
public class NewMain {
    private static final int TOTAL_QUEST = 10;
    private static final int RIGHT_BELOW_THRESHOLD_POINT = 10;
    private static final int RIGHT_ABOVE_THRESHOLD_POINT = 5;
    private static final int WRONG_POINT = 0;
    private static int totalTime = 0;
    private static int initScore = 100;
    private static Fraction realExpectedAnswer;

    public static void main(String[] args) {
        // write your code here
        Scanner scanner = new Scanner(System.in);
        String startNewQuestsIpt;
        QuestionMaker generateAns = new QuestionMaker();
        long thresholdTime;
        int totalRightBelowThreshold;
        int totalRightAboveThreshold;
        int totalWrong;
        ClockTimer timer = new ClockTimer();

        do {
            // initialize value
            startNewQuestsIpt = "";
            totalRightBelowThreshold = 0;
            totalRightAboveThreshold = 0;
            totalWrong = 0;

            // Asking for asnwering question threshold time
            System.out.print("How much time do you need "
                    + "to answer each question? (In second) ");
            String rawInput = scanner.nextLine();
            thresholdTime = rawInput.isEmpty() ? 20 : Integer.parseInt(rawInput);

            for (int questNo = 1; questNo <= TOTAL_QUEST; questNo++) {
                System.out.print(questNo + ") ");
                Thread threadAns = new Thread(generateAns);
                Thread threadTimer = new Thread(timer);
                threadAns.start();
                threadTimer.start();

                // Asking for question
                // And capture before and after the time in milis
                String rawAns = scanner.nextLine();
                int timeNeedForAns = timer.getCountForTime().get();

                // Process user answer
                Fraction userAnswer;
                if (rawAns.contains("/")) {
                    String[] ans = rawAns.split("/");
                    userAnswer = new Fraction(Integer.parseInt(ans[0]),
                            Integer.parseInt(ans[1]));
                } else {
                    userAnswer = new Fraction(Integer.parseInt(rawAns));
                }
                // To get expected answer from the QuestionMaker class
                realExpectedAnswer = generateAns.getExceptedAnswer();

                // Check answer
                if (realExpectedAnswer.isEqual(userAnswer)) {
                    if (timeNeedForAns <= thresholdTime) {
                        //totalRightBelowThreshold++;
                        initScore -= timer.getCountForScore().get();
                        initScore += timer.calculateAnsInsideThresHold(initScore);
                        printResult(timeNeedForAns, initScore);
                        totalTime += timeNeedForAns;
                    } else {
                        //totalRightAboveThreshold++;
                        initScore -= timer.getCountForScore().get();
                        initScore += timer.calculateAnsOutsideThresHold(initScore);
                        printResult(timeNeedForAns, initScore);
                        totalTime += timeNeedForAns;
                    }

                } else {
                    //totalWrong++;
                    initScore -= timer.getCountForScore().get();
                    printResult(timeNeedForAns, initScore);
                    totalTime += timeNeedForAns;
                }
            }

            // Print the result
            System.out.println("\n=========Result==========");
            System.out.println("Your Final Score: " + initScore);
            System.out.println("Total Time = " + totalTime);
            System.out.println("Right answer and within time limit  =  "
                    + totalRightBelowThreshold);
            System.out.println("Right answer but over time limit  =  "
                    + totalRightAboveThreshold);
            System.out.println("Wrong answer  =  " + totalWrong);

            int totalPoint = initScore + (totalRightBelowThreshold * RIGHT_BELOW_THRESHOLD_POINT)
                    + (totalRightAboveThreshold * RIGHT_ABOVE_THRESHOLD_POINT)
                    + (totalWrong * WRONG_POINT);
            System.out.println("\nTotal point acquired : " + totalPoint
                    + "(" + (totalRightBelowThreshold * RIGHT_BELOW_THRESHOLD_POINT)
                    + "+" + (totalRightAboveThreshold * RIGHT_ABOVE_THRESHOLD_POINT)
                    + "+" + (totalWrong * WRONG_POINT) + ")");
            // Asking if user want to start a new questions
            // if the respond is not what we want, ask it again and again
            while (!startNewQuestsIpt.equalsIgnoreCase("y")
                    && !startNewQuestsIpt.equalsIgnoreCase("n")) {
                System.out.println("Restart the quiz? [y/n]");
                startNewQuestsIpt = scanner.nextLine();
            }
            System.out.println("\n\n\n\n\n\n");
        } while (startNewQuestsIpt.equalsIgnoreCase("y"));
        // while user input yes, do same step again
    }

    public static void printResult(int timeNeed, int score) {
        System.out.println("Current Score: " + score);
        System.out.println("Time to Answer: " + timeNeed);
    }
}
