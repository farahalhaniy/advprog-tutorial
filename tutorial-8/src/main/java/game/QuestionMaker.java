package game;

import java.util.Random;

/**
 * Created by farah on 4/13/2018.
 */
public class QuestionMaker implements Runnable {
    private static final int QUEST_TYPE_ADD = 0;
    private static final int QUEST_TYPE_SUBSTR = 1;
    private static final int QUEST_TYPE_MULTIPL = 2;
    private static final int QUEST_TYPE_DIVS = 3;
    private Fraction expectedAnswer;

    public void run() {
        Random rand = new Random();
        Fraction firstPosFrac = new Fraction(rand.nextInt(40) - 20,
                rand.nextInt(40) - 20);
        Fraction secondPosFrac = new Fraction(rand.nextInt(40) - 20,
                rand.nextInt(40) - 20);


        switch (rand.nextInt(3)) {
            case QUEST_TYPE_ADD:
                System.out.print(firstPosFrac.toString() + "  +  "
                        + secondPosFrac.toString() + "  =  ");
                expectedAnswer = firstPosFrac.getAddition(secondPosFrac);
                break;
            case QUEST_TYPE_SUBSTR:
                System.out.print(firstPosFrac.toString() + "  -  "
                        + secondPosFrac.toString() + "  =  ");
                expectedAnswer = firstPosFrac.getSubstraction(secondPosFrac);
                break;
            case QUEST_TYPE_MULTIPL:
                System.out.print(firstPosFrac.toString() + "  *  "
                        + secondPosFrac.toString() + "  =  ");
                expectedAnswer = firstPosFrac.getMultiplication(secondPosFrac);
                break;
            case QUEST_TYPE_DIVS:
                System.out.print(firstPosFrac.toString() + "  :  "
                        + secondPosFrac.toString() + "  =  ");
                expectedAnswer = firstPosFrac.getDivision(secondPosFrac);
                break;
            default:
                System.out.println("Ooops!");
                expectedAnswer = new Fraction();
        }
    }

    public Fraction getExceptedAnswer() {
        return this.expectedAnswer;
    }
}
