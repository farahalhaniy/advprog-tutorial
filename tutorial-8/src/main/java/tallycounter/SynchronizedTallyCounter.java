package tallycounter;

/**
 * Created by farah on 4/13/2018.
 */
public class SynchronizedTallyCounter extends TallyCounter {
    private int counter;

    public SynchronizedTallyCounter() {
        this.counter = 0;
    }

    @Override
    public synchronized void increment() {
        this.counter++;
    }

    @Override
    public synchronized void decrement() {
        this.counter--;
    }

    @Override
    public synchronized int value() {
        return this.counter;
    }

}
