package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.Assert.assertEquals;

/**
 * Created by farah on 3/9/2018.
 */
public class DepokPizzaIngredientFactoryTest {
    private DepokPizzaIngredientFactory depokPizzaIngredientFactory;

    @BeforeEach
    void setUp() {
        depokPizzaIngredientFactory = new DepokPizzaIngredientFactory();
    }

    @Test
    void testCreateDough() {
        Dough dough = depokPizzaIngredientFactory.createDough();
        assertEquals("ThickCrust style extra thick crust dough", dough.toString());
    }

    @Test
    void testCreateSauce() {
        Sauce sauce = depokPizzaIngredientFactory.createSauce();
        assertEquals("Tomato sauce with plum tomatoes", sauce.toString());
    }

    @Test
    void testCreateCheese() {
        Cheese cheese = depokPizzaIngredientFactory.createCheese();
        assertEquals("Provolone Cheese", cheese.toString());
    }

    @Test
    void testCreateVeggies() {
        String[] veggies = {"Black Olives", "Eggplant", "Spinach", "Zucchini"};
        Veggies[] veggiesFinal = depokPizzaIngredientFactory.createVeggies();
        for(int i = 0; i < veggiesFinal.length; i++) {
            assertEquals(veggiesFinal[i].toString(), veggies[i]);
        }
    }

    @Test
    void testCreateClam() {
        Clams clams = depokPizzaIngredientFactory.createClam();
        assertEquals("Frozen Clams from Chesapeake Bay", clams.toString());
    }
}
