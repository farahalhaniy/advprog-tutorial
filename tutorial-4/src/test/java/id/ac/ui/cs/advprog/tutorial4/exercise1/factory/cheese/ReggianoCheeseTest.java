package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.Assert.assertEquals;

/**
 * Created by farah on 3/8/2018.
 */
public class ReggianoCheeseTest {
    private ReggianoCheese reggianoCheese;

    @BeforeEach
    void setUp() {
        reggianoCheese = new ReggianoCheese();
    }

    @Test
    void testToString() {
        String expected = "Reggiano Cheese";
        assertEquals(expected, reggianoCheese.toString());
    }
}
