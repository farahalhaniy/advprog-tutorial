package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by farah on 3/9/2018.
 */
public class ClamPizzaTest {
    private ClamPizza depokClamPizza;
    private ClamPizza nyClamPizza;

    @BeforeEach
    void setUp() {
        depokClamPizza = new ClamPizza(new DepokPizzaIngredientFactory());
        nyClamPizza = new ClamPizza(new NewYorkPizzaIngredientFactory());
    }

    @Test
    void testDepokClamPizza() {
        depokClamPizza.prepare();
        assertEquals(depokClamPizza.dough.toString(), "ThickCrust style extra thick crust dough");
        assertEquals(depokClamPizza.sauce.toString(), "Tomato sauce with plum tomatoes");
        assertEquals(depokClamPizza.cheese.toString(), "Provolone Cheese");
        assertEquals(depokClamPizza.clam.toString(), "Frozen Clams from Chesapeake Bay");
    }

    @Test
    void testNYClamPizza() {
        nyClamPizza.prepare();
        assertEquals(nyClamPizza.dough.toString(), "Thin Crust Dough");
        assertEquals(nyClamPizza.sauce.toString(), "Marinara Sauce");
        assertEquals(nyClamPizza.cheese.toString(), "Reggiano Cheese");
        assertEquals(depokClamPizza.clam.toString(), "Fresh Clams from Long Island Sound");
    }
}
