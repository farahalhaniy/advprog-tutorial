package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by farah on 3/9/2018.
 */
public class NewYorkPizzaIngredientFactoryTest {
    private NewYorkPizzaIngredientFactory newYorkPizzaIngredientFactory;

    @BeforeEach
    void setUp() {
        newYorkPizzaIngredientFactory = new NewYorkPizzaIngredientFactory();
    }

    @Test
    void testCreateDough() {
        Dough dough = newYorkPizzaIngredientFactory.createDough();
        assertEquals("Thin Crust Dough", dough.toString());
    }

    @Test
    void testCreateSauce() {
        Sauce sauce = newYorkPizzaIngredientFactory.createSauce();
        assertEquals("Marinara Sauce", sauce.toString());
    }

    @Test
    void testCreateCheese() {
        Cheese cheese = newYorkPizzaIngredientFactory.createCheese();
        assertEquals("Reggiano Cheese", cheese.toString());
    }

    @Test
    void testCreateVeggies() {
        String[] veggies = {"Garlic", "Onion", "Mushroom", "Red Pepper"};
        Veggies[] veggiesFinal = newYorkPizzaIngredientFactory.createVeggies();
        for(int i = 0; i < veggiesFinal.length; i++) {
            assertEquals(veggiesFinal[i].toString(), veggies[i]);
        }
    }

    @Test
    void testCreateClam() {
        Clams clams = newYorkPizzaIngredientFactory.createClam();
        assertEquals("Fresh Clams from Long Island Sound", clams.toString());
    }
}
