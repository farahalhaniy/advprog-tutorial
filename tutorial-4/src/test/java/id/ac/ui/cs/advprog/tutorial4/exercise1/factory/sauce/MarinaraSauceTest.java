package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.Assert.assertEquals;

/**
 * Created by farah on 3/8/2018.
 */
public class MarinaraSauceTest {
    private MarinaraSauce marinaraSauce;

    @BeforeEach
    void setUp() {
        marinaraSauce = new MarinaraSauce();
    }

    @Test
    void testToString() {
        String expected = "Marinara Sauce";
        assertEquals(expected, marinaraSauce.toString());
    }
}
