package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by farah on 3/8/2018.
 */
public class DepokPizzaStoreTest {
    private DepokPizzaStore depokPizzaStore;

    @BeforeEach
    void setUp() {
        depokPizzaStore = new DepokPizzaStore();
    }

    @Test
    void testCreatePizzaCheese() {
        Pizza pizza = depokPizzaStore.createPizza("cheese");
        assertEquals("Depok Style Cheese Pizza", pizza.getName());
    }

    @Test
    void testCreatePizzaVeggie() {
        Pizza pizza = depokPizzaStore.createPizza("veggie");
        assertEquals("Depok Style Veggie Pizza", pizza.getName());
    }

    @Test
    void testCreatePizzaClam() {
        Pizza pizza = depokPizzaStore.createPizza("clam");
        assertEquals("Depok Style Clam Pizza", pizza.getName());
    }
}
