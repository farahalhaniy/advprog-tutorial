package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.Assert.assertEquals;

/**
 * Created by farah on 3/8/2018.
 */
public class PlumTomatoSauceTest {
    private PlumTomatoSauce plumTomatoSauce;

    @BeforeEach
    void setUp() {
        plumTomatoSauce = new PlumTomatoSauce();
    }

    @Test
    void testToString() {
        String expected = "Tomato sauce with plum tomatoes";
        assertEquals(expected, plumTomatoSauce.toString());
    }
}
