package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.Assert.assertEquals;

/**
 * Created by farah on 3/8/2018.
 */
public class ParmesanCheeseTest {
    private ParmesanCheese parmesanCheese;

    @BeforeEach
    void setUp() {
        parmesanCheese = new ParmesanCheese();
    }

    @Test
    void testToString() {
        String expected = "Shredded Parmesan";
        assertEquals(expected, parmesanCheese.toString());
    }
}
