package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.Assert.assertEquals;

/**
 * Created by farah on 3/8/2018.
 */
public class CookedClamsTest {
    private CookedClams cookedClams;

    @BeforeEach
    void setUp() {
        cookedClams = new CookedClams();
    }

    @Test
    void testToString() {
        String expected = "Cooked Clams";
        assertEquals(expected, cookedClams.toString());
    }
}
