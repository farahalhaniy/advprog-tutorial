package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by farah on 3/8/2018.
 */
public class NewYorkPizzaStoreTest {
    private NewYorkPizzaStore newYorkPizzaStore;

    @BeforeEach
    void setUp() {
        newYorkPizzaStore = new NewYorkPizzaStore();
    }

    @Test
    void testCreatePizzaCheese() {
        Pizza pizza = newYorkPizzaStore.createPizza("cheese");
        assertEquals("New York Style Cheese Pizza", pizza.getName());
    }

    @Test
    void testCreatePizzaVeggie() {
        Pizza pizza = newYorkPizzaStore.createPizza("veggie");
        assertEquals("New York Style Veggie Pizza", pizza.getName());
    }

    @Test
    void testCreatePizzaClam() {
        Pizza pizza = newYorkPizzaStore.createPizza("clam");
        assertEquals("New York Style Clam Pizza", pizza.getName());
    }
}
