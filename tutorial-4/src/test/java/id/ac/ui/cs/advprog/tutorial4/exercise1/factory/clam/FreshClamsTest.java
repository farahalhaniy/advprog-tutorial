package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.Assert.assertEquals;

/**
 * Created by farah on 3/8/2018.
 */
public class FreshClamsTest {
    private FreshClams freshClams;

    @BeforeEach
    void setUp() {
        freshClams = new FreshClams();
    }

    @Test
    void testToString() {
        String expected = "Fresh Clams from Long Island Sound";
        assertEquals(expected, freshClams.toString());
    }
}
