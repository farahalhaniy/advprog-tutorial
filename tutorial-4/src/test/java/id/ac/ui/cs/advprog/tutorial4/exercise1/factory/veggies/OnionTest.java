package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.Assert.assertEquals;

/**
 * Created by farah on 3/8/2018.
 */
public class OnionTest {
    private Onion onion;

    @BeforeEach
    void setUp() {
        onion = new Onion();
    }

    @Test
    void testToString() {
        String expected = "Onion";
        assertEquals(expected, onion.toString());
    }
}
