package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.Assert.assertEquals;

/**
 * Created by farah on 3/8/2018.
 */
public class RedPepperTest {
    private RedPepper redPepper;

    @BeforeEach
    void setUp() {
        redPepper = new RedPepper();
    }

    @Test
    void testToString() {
        String expected = "Red Pepper";
        assertEquals(expected, redPepper.toString());
    }
}
