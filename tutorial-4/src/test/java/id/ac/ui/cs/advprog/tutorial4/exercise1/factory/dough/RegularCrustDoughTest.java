package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.Assert.assertEquals;

/**
 * Created by farah on 3/8/2018.
 */
public class RegularCrustDoughTest {
    private RegularCrustDough regularCrustDough;

    @BeforeEach
    void setUp() {
        regularCrustDough = new RegularCrustDough();
    }

    @Test
    void testToString() {
        String expected = "Regular Crust Dough";
        assertEquals(expected, regularCrustDough.toString());
    }
}
