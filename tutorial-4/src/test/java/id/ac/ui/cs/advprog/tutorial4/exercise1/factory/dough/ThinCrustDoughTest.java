package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.Assert.assertEquals;

/**
 * Created by farah on 3/8/2018.
 */
public class ThinCrustDoughTest {
    private ThinCrustDough thinCrustDough;

    @BeforeEach
    void setUp() {
        thinCrustDough = new ThinCrustDough();
    }

    @Test
    void testToString() {
        String expected = "Thin Crust Dough";
        assertEquals(expected, thinCrustDough.toString());
    }
}
