package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by farah on 3/9/2018.
 */
public class VeggiePizzaTest {
    private VeggiePizza depokVeggiePizza;
    private VeggiePizza nyVeggiePizza;

    @BeforeEach
    void setUp() {
        depokVeggiePizza = new VeggiePizza(new DepokPizzaIngredientFactory());
        nyVeggiePizza = new VeggiePizza(new NewYorkPizzaIngredientFactory());
    }

    @Test
    void testDepokClamPizza() {
        depokVeggiePizza.prepare();
        assertEquals(depokVeggiePizza.dough.toString(), "ThickCrust style extra thick crust dough");
        assertEquals(depokVeggiePizza.sauce.toString(), "Tomato sauce with plum tomatoes");
        assertEquals(depokVeggiePizza.cheese.toString(), "Provolone Cheese");

        String[] veggies = {"Black Olives", "Eggplant", "Spinach", "Zucchini"};
        Veggies[] veggiesFinal = depokVeggiePizza.veggies;
        for(int i = 0; i < veggiesFinal.length; i++) {
            assertEquals(veggiesFinal[i].toString(), veggies[i]);
        }
    }

    @Test
    void testNYClamPizza() {
        nyVeggiePizza.prepare();
        assertEquals(nyVeggiePizza.dough.toString(), "Thin Crust Dough");
        assertEquals(nyVeggiePizza.sauce.toString(), "Marinara Sauce");
        assertEquals(nyVeggiePizza.cheese.toString(), "Reggiano Cheese");

        String[] veggies = {"Garlic", "Onion", "Mushroom", "Red Pepper"};
        Veggies[] veggiesFinal = nyVeggiePizza.veggies;
        for(int i = 0; i < veggiesFinal.length; i++) {
            assertEquals(veggiesFinal[i].toString(), veggies[i]);
        }
    }
}
