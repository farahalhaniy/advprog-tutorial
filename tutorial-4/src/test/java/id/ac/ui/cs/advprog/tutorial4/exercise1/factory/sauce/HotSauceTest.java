package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.Assert.assertEquals;

/**
 * Created by farah on 3/8/2018.
 */
public class HotSauceTest {
    private HotSauce hotSauce;

    @BeforeEach
    void setUp() {
        hotSauce = new HotSauce();
    }

    @Test
    void testToString() {
        String expected = "Hot Sauce";
        assertEquals(expected, hotSauce.toString());
    }
}
