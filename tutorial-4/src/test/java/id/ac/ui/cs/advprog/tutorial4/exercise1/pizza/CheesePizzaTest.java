package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;


import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by farah on 3/9/2018.
 */
public class CheesePizzaTest {
    private CheesePizza depokCheesePizza;
    private CheesePizza nyCheesePizza;

    @BeforeEach
    void setUp() {
        depokCheesePizza = new CheesePizza(new DepokPizzaIngredientFactory());
        nyCheesePizza = new CheesePizza(new NewYorkPizzaIngredientFactory());
    }

    @Test
    void testDepokCheesePizza() {
        depokCheesePizza.prepare();
        assertEquals(depokCheesePizza.dough.toString(), "ThickCrust style extra thick crust dough");
        assertEquals(depokCheesePizza.sauce.toString(), "Tomato sauce with plum tomatoes");
        assertEquals(depokCheesePizza.cheese.toString(), "Provolone Cheese");
    }

    @Test
    void testNYCheesePizza() {
        nyCheesePizza.prepare();
        assertEquals(nyCheesePizza.dough.toString(), "Thin Crust Dough");
        assertEquals(nyCheesePizza.sauce.toString(), "Marinara Sauce");
        assertEquals(nyCheesePizza.cheese.toString(), "Reggiano Cheese");
    }
}
