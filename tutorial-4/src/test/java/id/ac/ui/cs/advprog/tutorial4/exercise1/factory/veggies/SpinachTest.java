package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by farah on 3/8/2018.
 */
public class SpinachTest {
    private Spinach spinach;

    @BeforeEach
    void setUp() {
        spinach = new Spinach();
    }

    @Test
    void testToString() {
        String expected = "Spinach";
        assertEquals(expected, spinach.toString());
    }
}
