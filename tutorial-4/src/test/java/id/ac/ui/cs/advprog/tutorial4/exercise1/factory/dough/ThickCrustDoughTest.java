package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.Assert.assertEquals;

/**
 * Created by farah on 3/8/2018.
 */
public class ThickCrustDoughTest {
    private ThickCrustDough thickCrustDough;

    @BeforeEach
    void setUp() {
        thickCrustDough = new ThickCrustDough();
    }

    @Test
    void testToString() {
        String expected = "ThickCrust style extra thick crust dough";
        assertEquals(expected, thickCrustDough.toString());
    }
}
