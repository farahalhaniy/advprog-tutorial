package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.Assert.assertEquals;

/**
 * Created by farah on 3/8/2018.
 */
public class ProvoloneCheeseTest {
    private ProvoloneCheese provoloneCheese;

    @BeforeEach
    void setUp(){
        provoloneCheese = new ProvoloneCheese();
    }

    @Test
    void testToString() {
        String expected = "Provolone Cheese";
        assertEquals(expected, provoloneCheese.toString());
    }
}
