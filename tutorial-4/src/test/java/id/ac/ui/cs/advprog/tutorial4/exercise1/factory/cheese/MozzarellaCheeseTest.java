package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;


/**
 * Created by farah on 3/8/2018.
 */
public class MozzarellaCheeseTest {
    private MozzarellaCheese mozzarellaCheese;

    @BeforeEach
    void setUp() {
        mozzarellaCheese = new MozzarellaCheese();
    }

    @Test
    void testToString() throws Exception {
        String expected = "Shredded Mozzarella";
        assertEquals(expected, mozzarellaCheese.toString());
    }
}




