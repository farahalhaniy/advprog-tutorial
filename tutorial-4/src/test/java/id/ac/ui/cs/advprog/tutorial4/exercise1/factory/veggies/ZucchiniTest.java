package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.Assert.assertEquals;

/**
 * Created by farah on 3/8/2018.
 */
public class ZucchiniTest {
    private Zucchini zucchini;

    @BeforeEach
    void setUp() {
        zucchini = new Zucchini();
    }

    @Test
    void testToString() {
        String expected = "Zucchini";
        assertEquals(expected, zucchini.toString());
    }
}
