package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.Assert.assertEquals;

/**
 * Created by farah on 3/8/2018.
 */
public class FrozenClamsTest {
    private FrozenClams frozenClams;

    @BeforeEach
    void setUp() {
        frozenClams = new FrozenClams();
    }

    @Test
    void testToString() {
        String expected = "Frozen Clams from Chesapeake Bay";
        assertEquals(expected, frozenClams.toString());
    }
}
