package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.Assert.assertEquals;

/**
 * Created by farah on 3/8/2018.
 */
public class EggplantTest {
    private Eggplant eggplant;

    @BeforeEach
    void setUp() {
        eggplant = new Eggplant();
    }

    @Test
    void testToString() {
        String expected = "Eggplant";
        assertEquals(expected, eggplant.toString());
    }
}
