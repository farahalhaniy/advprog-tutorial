package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

/**
 * Created by farah on 3/8/2018.
 */
public class HotSauce implements Sauce{
    public String toString() {
        return "Hot Sauce";
    }
}
