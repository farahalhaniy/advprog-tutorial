package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

/**
 * Created by farah on 3/8/2018.
 */
public class RegularCrustDough implements Dough {
    public String toString() {
        return "Regular Crust Dough";
    }
}
