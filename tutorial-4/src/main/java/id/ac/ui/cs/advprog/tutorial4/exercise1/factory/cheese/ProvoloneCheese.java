package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

/**
 * Created by farah on 3/8/2018.
 */
public class ProvoloneCheese implements Cheese {
    public String toString() {
        return "Provolone Cheese";
    }
}
