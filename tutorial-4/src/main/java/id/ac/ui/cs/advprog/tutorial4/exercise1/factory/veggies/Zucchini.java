package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

/**
 * Created by farah on 3/8/2018.
 */
public class Zucchini implements Veggies {
    public String toString() {
        return "Zucchini";
    }
}
