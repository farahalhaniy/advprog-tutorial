package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

/**
 * Created by farah on 3/8/2018.
 */
public class CookedClams implements Clams {
    public String toString() {
        return "Cooked Clams";
    }
}
