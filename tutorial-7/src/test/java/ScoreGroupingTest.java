import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;

public class ScoreGroupingTest {
    // TODO Implement me!
    // Increase code coverage in ScoreGrouping class
    // by creating unit test(s)!
    private static Map<String, Integer> scores;

    @Before
    public void setUp() {
        scores = new HashMap<String, Integer>();
        scores.put("A", 12);
        scores.put("B", 14);
        scores.put("C", 10);
        scores.put("D", 14);
        scores.put("E", 14);
        scores.put("F", 11);
    }

    @Test
    public void testGroupByScore() {
        assertEquals(ScoreGrouping.groupByScores(scores).toString(),
                "{10=[C], 11=[F], 12=[A], 14=[B, D, E]}");
    }
}